'''
Function as described in HW5, and the inverse.
'''

from typing import Tuple


def n_to_k_l(n: int) -> Tuple[int, int]:
    '''
    Takes a number n and finds (k, l) such that n = 2^k * (2l + 1)

    This function uses some bit hacks.
    * n & 1 is equivalent to n % 2, but is O(1) instead of O(n^2)
    * n >>= 1 is equivalent to n //= 2, but is O(1) instead of O(n^2)
    * (n - 1) >> 1 is equivalent to (n - 1) // 2, but is O(2) instead of
      O(1 + n^2)

    These reductions in time are *significant*, and I think this is likely the
    fastest program you could write in Python, totaling to O(log(n)) due to
    the while looping.
    '''
    if n < 1:
        raise Exception('n must be a natural number.')
    k: int = 0
    while not n & 1:
        n >>= 1
        k += 1
    l: int = (n - 1) >> 1
    return (k, l)


def k_l_to_n(k_l: Tuple[int, int]) -> int:
    '''
    Takes a tuple(k, l) and returns 2^k * (2l + 1)

    This function uses some bit hacks.
    * 1 << k is equivalent to 2**k but is O(1) on most processors
        2**k is likely close to O(1), but 1 << k is for sure O(1) if the
        hardware supports it
    * l << 1 is equivalent to l * 2 but is O(1) instead of O(n log(n))

    I get a speed-up of about 1 min when using `hw_5_check.py` with
    n = 1_000_000_000 using this method as opposed to 2**k * (2 * l + 1).
    This is not nearly as dramatic as the `n_to_k_l` function but is
    interesting regardless.
    '''
    k: int
    l: int
    k, l = k_l
    return (1 << k) * ((l << 1) + 1)
