#!/bin/python3
'''
A small program that lets you test any function for correct (k, l) up to some
number.
'''

from typing import Callable, cast, Iterable, Tuple
import sys
from hw_5 import n_to_k_l, k_l_to_n


def test_function_up_to_n(
        f: Callable[[int], Tuple[int, int]],
        n: int) -> None:
    it_works: bool = all(
        [k_l_to_n(f(x)) == x
         for x in
         cast(Iterable[int], range(1, n))]
    )
    if it_works:
        print('The function works for numbers up to ' + str(n))
    else:
        print('The function does not work for numbers up to ' + str(n))


def main() -> int:
    good_input: bool = False
    tries: int = 3
    while tries and not good_input:
        try:
            n: int = int(
                input('Enter a number to test up to [1+]: ')
            )
            if n < 1:
                raise ValueError
        except ValueError:
            tries -= 1
            print('n must be a natural number')
        else:
            good_input = True

    if not tries and not good_input:
        return 1
    else:
        test_function_up_to_n(n_to_k_l, n)
        return 0


if __name__ == '__main__':
    sys.exit(main())
