# MATH369 HW#5

## What is This Repository

### Assignment

> Write a function $`f(n)`$ [where $`n`$ is a natural number] that returns the pair $`(k, l)`$ such that
>```math
>n = (2^k)(2l+1)
>```
### Relevant Code

```python
def n_to_k_l(n: int) -> Tuple[int, int]:
    '''
    Takes a number n and finds (k, l) such that n = 2^k * (2l + 1)

    This function uses some bit hacks.
    * n & 1 is equivalent to n % 2, but is O(1) instead of O(n^2)
    * n >>= 1 is equivalent to n //= 2, but is O(1) instead of O(n^2)
    * (n - 1) >> 1 is equivalent to (n - 1) // 2, but is O(2) instead of
      O(1 + n^2)

    These reductions in time are *significant*, and I think this is likely the
    fastest program you could write in Python, totaling to O(log(n)) due to
    the while looping.
    '''
    if n < 1:
        raise Exception('n must be a natural number.')
    k: int = 0
    while not n & 1:
        n >>= 1
        k += 1
    l: int = (n - 1) >> 1
    return (k, l)
```
### Code Justification

One has to realize that the only way for $`n`$ to be odd (which is a case that must be included, of course) is when $`k=0`$, then $`n=2l+1`$ which is the definition of an odd number.

For even numbers, we can do something a little different.
$`2^k`$ represents the number $`k`$ of prime factors $`2`$ in $`n`$, and $`(2l+1)`$ is another factor (potentially not prime, but it doesn't matter) which is an odd number in $`n`$, and after you factor out $`2^k`$ you just need to work backwards to find $`l`$ like you would for an odd $`n`$.

## How to Use This Repository

My code along with the function inverse is in `hw_5.py`, and there are testing programs you can use in `hw_5_check.py` and `hw_5_time.py` to check the function correctness and time to execute, respectively.

If you just want to copy/paste my function $`f(n)`$, it is provided above in the "Relevant Code" subsection.

to test the function correctness (or to modify the testing program to use another function):

1. Clone/download this repository
2. `cd` to the `src` directory
3. (*Optional*) To change the function to test:
    * import the function that you want to test in `hw_5_check.py`
    * change the call to `test_function_up_to_n(f, n)` at the bottom of `main()` so that `f` is the new function to test
4. ```bash
   $ python3 hw_5_check.py
   ```
5. follow the prompt

To test the time to execute a function (or to modify the testing program to use another function):
1. follow steps 1 & 2 from the above procedure
2. (*Optional*) To change the function to time:
    * import the function that you want to test in `hw_5_time.py`
    * change the call to `time_function_for_n(f, n)` at the bottom of `main()` so that `f` is the new function to time
3. ```bash
   $ python3 hw_5_time.py
   ```
  4. follow the prompt

## My Findings

I've tested my function using `hw_5_check.py` up to $`n=1,000,000,000`$ and found that it works, taking $`7`$ minutes and $`37`$ seconds to test all values.
I have a pretty nice desktop computer (and your patience is likely lower than mine 🙂), so you will probably want to choose a lower $`n`$ for your test limit.

When timing the function using `hw_5_time.py` my program generates the following table using my desktop computer:

```txt
Enter a max p to time n=10^p [0+]: 12

n                          iterations                 seconds/iteration
--------------------------------------------------------------------------------
1                          2000000                    1.614349379669875e-07
10                         1000000                    2.638851230731234e-07
100                        1000000                    3.188287660013884e-07
1000                       1000000                    3.832856839289889e-07
10000                      500000                     4.5853567216545343e-07
100000                     500000                     5.128903479781002e-07
1000000                    500000                     5.688790380954742e-07
10000000                   500000                     6.258960459381342e-07
100000000                  500000                     6.811897580046207e-07
1000000000                 500000                     7.365606680978089e-07
10000000000                500000                     8.068824200890958e-07
100000000000               500000                     8.768863738514482e-07
1000000000000              200000                     1.0863250400871039e-06
```
This only takes a few seconds to run, so don't be afraid to use other functions in here or to raise the p value.
This seems to confirm my earlier thoughts that the order of $`f(n)`$ is $`O(log(n))`$.